module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),

    concat: {
      options: {
        separator: ';'
      },
      dist: {
        src: ['components/**/*.js'],
        dest: 'assets/scripts.js'
      }
    }

    uglify: {
      dist: {
        files: {
          'assets/js/scripts.min.js': ['components/js/*.js']
        }
      }
    }, // uglify

    watch: {
      options: {
        livereload: true,
      },
      css: {
        files: [
          '**/*.sass',
          '**/*.scss'
        ],
        tasks: ['compass']
      },
      js: {
        files: [
          'assets/js/*.js',
          'Gruntfile.js'
        ],
        tasks: ['jshint']
      },
      html: {
        files: [
          '*.html'
        ]
      }
    },
    compass: {
      dist: {
        options: {
          sassDir: 'components/sass',
          cssDir: 'assets/css',
          outputStyle: 'compressed'
        }
      }
    },
    
    'bower-install': {

        target: {

          // when you run `grunt bower-install`
          html: 'index.html',

          //   default: '<link rel="stylesheet" href="{{filePath}}" />'
          cssPattern: '<link href="{{filePath}}" rel="stylesheet">',

          // Customize how your <script>s are included into
          // your HTML file.
          //
          //   default: '<script src="{{filePath}}"></script>'
          jsPattern: '<script type="text/javascript" src="{{filePath}}"></script>',
        }
      }
  });

  // Load the Grunt plugins.
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-compass');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-bower-install');

  // Register the default tasks.
  grunt.registerTask('default', ['watch']);
};